public class Main {
    public static void main(String[] args) {

        Game game1 = new Game("Ведьмак", 20, 100);
        Game game2 = new Game("Ведьмак", 20, 100);
        Game game3 = new Game("Hogwarts Legacy", 30, 20);

        System.out.println("Равны ли первая и вторая игры:" + game1.equals(game2));
        System.out.println("Равны ли первая и третья игры:" + game1.equals(game3));
        System.out.println("Хэшкоды: \n Первой игры - " + game1.hashCode()
                +"\n Второй игры - " + game2.hashCode()
                +"\n Третьей игры - " + game3.hashCode());
    }


}