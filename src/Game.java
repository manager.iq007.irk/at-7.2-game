import java.util.Random;

public class Game {
    String title;
    int price;
    int hoursPlayed;
    static String owner = "Настя";

    public Game(String title, int price, int hoursPlayed) {
        this.title = title;
        this.price = price;
        this.hoursPlayed = hoursPlayed;
    }

    @Override
    public String toString() { //Вывод информации об игре
        return "Название игры: " + this.title
                + "\n Цена в Steam - " + this.price + "$"
                + "\n Отыграно часов - " + this.hoursPlayed
                + "\n Владелец игры в библиотеке Steam- " + owner;
    }

    @Override
    public int hashCode() { //Оверрайд метода hashcode
        return this.title.charAt(0) + price + hoursPlayed;
    }

    @Override
    public boolean equals(Object g) { //Оверрайд метода equals
        if (g == this) return true;
        if (g == null || g.getClass() != this.getClass()) return false;
        Game testGame = (Game) g;
        if  (testGame.title == null) return false;
        return this.title.length() == testGame.title.length() && (this.title.equals(testGame.title) || this.price == testGame.price ||
                this.hoursPlayed == testGame.hoursPlayed);
    }

    public static int generatePrice() {
        Random random = new Random();
        return random.nextInt(1, 100);
    }

    public static int generateHours() {
        Random random = new Random();
        return random.nextInt(1, 100);
    }

    public void worthValue() {
        double i = (double) this.price / this.hoursPlayed;
        if (i <= 0.9) {
            System.out.println("Игра стоит своих денег!");
        } else {
            System.out.println("Игра не очень хороша :(");
        }
    }

    public static String generateGame() {
        String[] gamesTitles = {"The Witcher", "Plate up", "Cult of the Lamb", "Hogwarts Legacy", "Genshin Impact", "Dragon Age"};
        return gamesTitles[new Random().nextInt(gamesTitles.length)];
    }

    public static Game[] generateGames(int amount) {
        Game[] generatedGames = new Game[amount];
        for (int i = 0; i < generatedGames.length; i++) {
            generatedGames[i] = new Game(Game.generateGame(), Game.generatePrice(), Game.generateHours());
        }
        return generatedGames;
    }

}
